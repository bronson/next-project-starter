import { NextPage } from "next";
import Head from "next/head";
import { useEffect } from "react";
import $api from "../api"

const Home:NextPage = () => {

    useEffect(()=>{
        $api.getBackgroundImage().then(res=>{
            console.log(res,'res')
        })
    },[])

    return <>
        <div className="main-container">
            <Head>
                <title>基于NextJs的个人技术博客</title>
                <meta name="description" content="Next的进阶之路" />
                <meta property="og:title" content="Next - 个人技术网站" key="title" />
            </Head>
            <div>你好啊，NextJS</div>
        </div>
    </>
}


export default Home