## Next Project Starter

### 介绍

项目为 Next 的初始化项目，集成了 Axios，react-redux，redux-saga 等插件。后续会持续更新一些常用的插件和方法。希望大家能多多提出意见，一起学习，共同进步。

### 安装

```
git clone https://gitee.com/bronson/next-project-starter.git
cd next-project-starter
npm install
npm run dev
```
