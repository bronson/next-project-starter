/**
 * @desc API接口统一管理
 * @author Bronson
 * @create_date 2022-01-23
 * */ 

import { getBackgroundImage } from "./modules/Home"

export default{
    getBackgroundImage
}