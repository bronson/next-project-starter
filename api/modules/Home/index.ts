import service from "../../../utils/request"

/**
 * @desc 获取首页背景图片
 * @author Bronson
 * @create_date 2022-01-23
 * */ 
export function getBackgroundImage(params?:any){
    return service.get("/api/v1/getBgImage",{
        params:params
    })
}
