import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";

const statusMsg : any = {
    200: '查询成功',
    201: '操作成功',
    204: '操作成功',
    400: '参数错误',
    401: '没有权限',
    403: '禁止访问',
    404: '请求不存在',
    406: '请求格式错误',
    410: '资源被删除',
    500: '服务器发生错误'
}

// 创建axios实例
const instance = axios.create({ timeout: 1000 * 30 })
// 设置axios请求地址
instance.defaults.baseURL = "http://127.0.0.1:7001"
// 请求拦截器
instance.interceptors.request.use(
    (config:AxiosRequestConfig)=>{
        // 如果接口需要用到access_token，可以在这里处理；
        // const token = localStorage.getItem("token")
        // if(config.headers){
        //     token && (config.headers.Authorization = "Bearer" + token)
        // }
        return config
    },error=>{
        return Promise.reject(error)
    }
)

// 响应拦截器
instance.interceptors.response.use((response:AxiosResponse)=>{
    console.log(response,'--->request.ts: ---> response')
    const res = response.data
    return Promise.resolve({
        status:response.status,
        message:statusMsg[response.status],
        data:res.data
    })
},(error:AxiosError)=>{
    console.log(error,'---> request.js: ---> error')
    return Promise.resolve({
        status:error.code,
        message:"获取数据失败，请检查网络后再试",
        data:""
    })
})


export default instance;